variable "agent_namespace" {
  type    = string
  default = "gitlab-agent"
}

variable "apps_namespace" {
  type    = string
  default = "gitops-apps"
}

data "google_client_config" "current" {}

provider "kubernetes" {
  host                   = google_container_cluster.primary.endpoint
  cluster_ca_certificate = base64decode(google_container_cluster.primary.master_auth.0.cluster_ca_certificate)
  token                  = data.google_client_config.current.access_token
}

resource "kubernetes_namespace" "gitlab-agent" {
  metadata {
    annotations = {
      name = var.agent_namespace
    }
    name = var.agent_namespace
  }
}

resource "kubernetes_namespace" "gitops-apps" {
  metadata {
    annotations = {
      name = var.apps_namespace
    }
    name = var.apps_namespace
  }
}

resource "kubernetes_secret" "gitlab-agent-token" {
  metadata {
    name      = "gitlab-agent-token"
    namespace = kubernetes_namespace.gitlab-agent.metadata[0].name
  }

  data = {
    token = graphql_mutation.agent_token.computed_read_operation_variables.secret
  }

}

resource "kubernetes_service_account" "gitlab-agent" {
  metadata {
    name      = "gitlab-agent"
    namespace = kubernetes_namespace.gitlab-agent.metadata[0].name
  }
  secret {
    name = kubernetes_secret.gitlab-agent-token.metadata.0.name
  }
}

resource "kubernetes_deployment" "gitlab-agent" {
  wait_for_rollout = false
  metadata {
    name      = "gitlab-agent"
    namespace = kubernetes_namespace.gitlab-agent.metadata[0].name
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "gitlab-agent"
      }
    }

    template {
      metadata {
        labels = {
          app = "gitlab-agent"
        }
      }

      spec {
        service_account_name = kubernetes_service_account.gitlab-agent.metadata[0].name
        container {
          image = "registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/agentk:stable"
          name  = "agent"
          args = [
            "--token-file=/config/token",
            "--kas-address",
            "wss://kas.gitlab.com",
          ]
          volume_mount {
            name       = "token-volume"
            mount_path = "/config"
          }
        }

        volume {
          name = "token-volume"
          secret {
            secret_name = kubernetes_secret.gitlab-agent-token.metadata.0.name
          }
        }
      }
    }
    strategy {
      type = "RollingUpdate"
      rolling_update {
        max_surge       = 0
        max_unavailable = 1
      }
    }
  }
}

resource "kubernetes_cluster_role" "gitlab-agent-write" {
  metadata {
    name = "gitlab-agent-write"
  }

  rule {
    api_groups = ["*"]
    resources  = ["*"]
    verbs      = ["create", "update", "delete", "patch"]
  }
}

resource "kubernetes_cluster_role_binding" "gitlab-agent-write-binding" {
  metadata {
    name = "gitlab-agent-write-binding"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.gitlab-agent-write.metadata[0].name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.gitlab-agent.metadata[0].name
    namespace = kubernetes_namespace.gitlab-agent.metadata[0].name
  }
}

resource "kubernetes_cluster_role" "gitlab-agent-read" {
  metadata {
    name = "gitlab-agent-read"
  }

  rule {
    api_groups = ["*"]
    resources  = ["*"]
    verbs      = ["get", "list", "watch"]
  }
}

resource "kubernetes_cluster_role_binding" "gitlab-agent-read-binding" {
  metadata {
    name = "gitlab-agent-read-binding"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.gitlab-agent-write.metadata[0].name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.gitlab-agent.metadata[0].name
    namespace = kubernetes_namespace.gitlab-agent.metadata[0].name
  }
}

resource "kubernetes_cluster_role_binding" "gitlab-agent-cluster-admin" {
  # Getting errors without this.
  metadata {
    name = "gitlab-agent-cluster-admin"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.gitlab-agent.metadata[0].name
    namespace = kubernetes_namespace.gitlab-agent.metadata[0].name
  }
}
