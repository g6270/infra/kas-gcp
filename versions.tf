terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
    graphql = {
      source = "sullivtr/graphql"
    }
  }
  required_version = "~> 1.0.0"
}
